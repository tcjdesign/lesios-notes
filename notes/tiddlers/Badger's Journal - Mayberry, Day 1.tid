created: 20201204182709134
creator: AWR
modified: 20201204210015246
modifier: AWR
tags: [[Badger's Journal]] [[Boddynock "Badger" Dergell]] [[Act 3 - A Wider World]]
title: Badger's Journal - Mayberry, Day 1
type: text/vnd.tiddlywiki

Just picked this journal up today. Always see [[Marearis|Cecilia "Marearis" Aquellarus]] scribbling in hers, seemed interesting. Not much of a writer, never have been, but what we've been through lately is worth writing down. Never thought I'd get involved in something this big again, never figured on much of any of this.

Starting at the beginning, just to make sense of it all. Finally worked up the nerve to leave the [[Veiled Forest]]. Going to miss [[Romero]] and the others, but we all knew I had to go. That was the place I was put back together, at least a bit, but it wasn't home. Wandered around for a bit, eventually stumbling into a little place called [[Barley Town]]. Out in the sticks, no way these people got many [[Gnomes]] in those parts, but they were kind folk. Seems a long time ago now, even though it's just been a few weeks.

Headed to the tavern to rest in a bed for once, and ended up running into my current compatriots. I'll write some notes on them all later, for now let's wrap up the recap.

Long story short, after a few odd jobs around town, it turns out the [[Corinthians|Corinthia]] are hunting for some sort of artifacts, and there's some insane spider cult of [[Lolth]] helping them out. In the middle of it all is now us, our little band of merry adventurers, contracted by the [[Oxmer]] to get these artifacts before the [[Corinthians|Corinthia]] can.

That search has brought us to [[Mayberry]]. By all rights, what should be a lovely town. [[Corinthian|Corinthia]] executions mar its beauty a bit, though. We're supposed to be keeping a low profile as we find our way to [[Piraeus]]. We need to book passage to there for the old Dwarven capital, and need to keep prying eyes away if we're to beat the [[Corinthians|Corinthia]] to the artifact there.

But what does [[Aequitas]] do as soon as we enter the city? ''March'' up on the stage of an execution, surrounded by some forty-odd soldiers, and demand of the general to let his prisoners go.

Truth be told, I wanted to do the same. They were killing a child, and for the crime of the father. __Butchers__, all of them. They'll not get they're hands on this artifact as long as I breathe. But I know the fight, the way it has to be fought. Once someone's been caught, sometimes there's just no getting them back. They have to be left behind, so the rest can go on. 

But it was a child.

Again.

It's cold, I know. Everything inside me tells me to stop fighting, to run up on that stage. To go out kicking and screaming trying to do what's right. But I can't. Because then I won't be around to make sure ''__they don't get away with it__''.

I respect [[Aequitas]] for what he did. It was stupid, but it was the best kind of stupid. I'm glad [[Marearis|Cecilia "Marearis" Aquellarus]] was able to talk him out of there. She is impressive, that one. Never thought I'd see the day someone talked their way out of a ring of soldiers. But they're both burned now, the [[Corinthians|Corinthia]] will be looking for them, and we can't leave this city until we get someone to sign off on our travel. We'll have to be even more cautious than before.

That's enough for now. Too tired to write more.