# Getting Started

You'll need a git client (I like [GitKraken](https://www.gitkraken.com/)) and access to the [GitLab repository](https://gitlab.com/tcjdesign/lesios-notes).

You'll need [nodejs](https://nodejs.org/en/) installed on your system. You'll also need yarn, which you can install with `npm install -g yarn` in command prompt after installing node.

Once you've got that set up, clone the wiki to a folder on your computer. 

## Running the Wiki Locally

Use git to make a new branch, name it something sensible like `Ander-5-19-20`.

Open a terminal window in the folder you cloned the repository to.

Run `yarn` from the terminal to install the dependencies, you'll only need to do this once.

Run `yarn wiki` to serve the wiki locally. You can now access it from your web browser at [localhost:8080](http://localhost:8080).

## Committing Your Changes

All of your changes will be saved to disk automatically. When you're done, close the wiki, commit your changes, push your branch, and open a merge request to merge your branch to the master branch. Unless you did something weird there won't be any conflicts and you can immediately accept your merge request - be sure to delete the branch when you're done.

# Viewing the Wiki Online

You can find the latest version of the wiki online [here](https://lesios.tcj.design). Everytime the master branch is changed, the wiki is rebuild and published online.

You cannot make changes directly from the online page.
